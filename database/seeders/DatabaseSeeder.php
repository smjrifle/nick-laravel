<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Enums\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('website')->count() == 0) {
            for ($i = 1; $i < 5; $i++) {
                DB::table('website')->insert([
                    'website_name' => 'Website ' . $i,
                    'status' => Status::Active,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
            }
        }
        if (DB::table('post')->count() == 0) {
            for ($i = 1; $i < 10; $i++) {
                DB::table('post')->insert([
                    'title' => 'Post ' . $i,
                    'description' => Str::random(mt_rand(200,500)),
                    'website_id' => mt_rand(1,5),
                    'status' => Status::Active,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
            }
        }
    }
}
