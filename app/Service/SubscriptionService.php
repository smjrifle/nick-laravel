<?php

namespace App\Service;

use App\Helper\Helper;
use App\Repository\Interfaces\ISubscriptionRepository;
use App\Service\Interfaces\ISubscriptionService;
use Exception;
use App\Models\Subscription;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Enums\Status;
use App\Exceptions\ClientException;
use App\Enums\Message;
use App\Http\Requests\SubscriptionRequest;

class SubscriptionService implements ISubscriptionService
{
    public function __construct(ISubscriptionRepository $subscriptionRepository)
    {
        $this->helper = new Helper();
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function add(Subscription $subscription)
    {
        try {
            DB::beginTransaction();
            $subscription_validation = $this->subscriptionRepository->getWhere([['email_id', $subscription->email_id], ['website_id', $subscription->website_id]])->first();
            if ($subscription_validation) {
                $subscription_validation->status = Status::Active;
                $subscription_validation->save();
                DB::commit();
                return $subscription_validation;
            }
            $subscription->save();
            DB::commit();
            return $subscription_validation;
        } catch (ClientException $e) {
            DB::rollBack();
            throw new ClientException($e->getMessage());
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            throw new Exception(Message::FAILED);
        }
    }

    public function toggle($id, $status = null)
    {
        try {
            DB::beginTransaction();
            $subscription  = $this->subscriptionRepository->get($id);
            if ($status) {
                $subscription->status == $status;
            } else {
                if ($subscription->status == Status::Active) {
                    $subscription->status = Status::Inactive;
                } else {
                    $subscription->status = Status::Active;
                }
            }
            $subscription->save();
            DB::commit();
            return $subscription;
        } catch (ClientException $e) {
            DB::rollBack();
            throw new ClientException($e->getMessage());
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception(Message::FAILED);
        }
    }
}
