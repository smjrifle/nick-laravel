<?php

namespace App\Service;

use App\Enums\Message;
use App\Exceptions\ClientException;
use App\Helper\Helper;
use App\Models\Website;
use App\Repository\Interfaces\IWebsiteRepository;
use App\Service\Interfaces\IWebsiteService;
use Exception;
use Illuminate\Support\Facades\DB;

class WebsiteService implements IWebsiteService
{

    public function __construct(IWebsiteRepository $websiteRepository)
    {
        $this->helper = new Helper();
        $this->websiteRepository = $websiteRepository;
    }

    public function add(Website $website){
        try {
            DB::beginTransaction();
            $website->save();
            DB::commit();
            return $website;
        }
        catch (ClientException $e) {
            DB::rollBack();
            throw new ClientException($e->getMessage());
        }
        catch (Exception $e) {
            DB::rollBack();
            throw new Exception(Message::FAILED);
        }
    }
}
