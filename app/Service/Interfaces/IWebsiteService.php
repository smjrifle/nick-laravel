<?php

namespace App\Service\Interfaces;

use App\Models\Website;

interface IWebsiteService
{
    public function add(Website $website);
}
