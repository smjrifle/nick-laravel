<?php

namespace App\Service\Interfaces;
use App\Models\Post;

interface IPostService
{
    public function add(Post $post);
}
