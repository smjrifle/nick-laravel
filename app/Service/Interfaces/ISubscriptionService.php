<?php

namespace App\Service\Interfaces;
use App\Models\Subscription;

interface ISubscriptionService
{
    public function add(Subscription $Subscription);
    public function toggle($id);
}
