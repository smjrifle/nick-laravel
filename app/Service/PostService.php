<?php

namespace App\Service;

use App\Helper\Helper;
use App\Repository\Interfaces\IPostRepository;
use App\Service\Interfaces\IPostService;
use Exception;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ClientException;
use App\Enums\Message;
use App\Jobs\NotificationJob;

class PostService implements IPostService
{
    public function __construct(IPostRepository $postRepository)
    {
        $this->helper = new Helper();
        $this->postRepository = $postRepository;
    }
    public function add(Post $post)
    {
        try {
            DB::beginTransaction();
            $post->save();
            DB::commit();
            dispatch(new NotificationJob($post));
            return $post;
        } catch (ClientException $e) {
            DB::rollBack();
            throw new ClientException($e->getMessage());
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception(Message::FAILED);
        }
    }
}
