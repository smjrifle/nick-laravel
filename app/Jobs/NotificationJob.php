<?php

namespace App\Jobs;

use App\Enums\Status;
use App\Mail\NotificationMail;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $post;
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->post->load(['website']);
        $subscriptionList = Subscription::where([['website_id',$this->post->website_id],['status',Status::Active]])->get();
        foreach($subscriptionList as $subscription){
            Mail::to($subscription->email_id)->send(new NotificationMail($this->post));
        }
    }
}
