<?php

namespace App\Repository;

use App\Models\Website;
use App\Repository\Interfaces\IWebsiteRepository;

class WebsiteRepository implements IWebsiteRepository
{

    public function get($id)
    {
        return Website::find($id);
    }

    public function all()
    {
        return Website::all();
    }

    public function delete($id)
    {
        return Website::destroy($id);
    }

    public function update($id, array $data)
    {
        return Website::find($id)->update($data);
    }

    public function getWhere($query = [])
    {
        return  Website::where($query);
    }
}
