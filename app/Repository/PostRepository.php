<?php

namespace App\Repository;

use App\Models\Post;
use App\Repository\Interfaces\IPostRepository;

class PostRepository implements IPostRepository
{

    public function get($id)
    {
        return Post::find($id);
    }

    public function all()
    {
        return Post::all();
    }

    public function delete($id)
    {
        return Post::destroy($id);
    }

    public function update($id, array $data)
    {
        return Post::find($id)->update($data);
    }

    public function getWhere($query = [])
    {
        return  Post::where($query);
    }
}
