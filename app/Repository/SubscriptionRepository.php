<?php

namespace App\Repository;

use App\Models\Subscription;
use App\Repository\Interfaces\ISubscriptionRepository;

class SubscriptionRepository implements ISubscriptionRepository
{


    public function get($id)
    {
        return Subscription::find($id);
    }

    public function all()
    {
        return Subscription::all();
    }

    public function delete($id)
    {
        return Subscription::destroy($id);
    }

    public function update($id, array $data)
    {
        return Subscription::find($id)->update($data);
    }

    public function getWhere($query = [])
    {
        return Subscription::where($query);
    }

}
