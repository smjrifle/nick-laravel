<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class Message extends Enum
{
    //ERROR
    const FAILED = "Service Down.Please try again later!!!";
    const UNAUTHORIZED = "Unauthorized access";
    const NOT_FOUND = "Searched data not found";
    const INVALID_DATA = "Invalid input data";

    //OTHER
    const ADDED = "Data has been added";
    const UPDATED = "Data has been updated";
    const FETCHED = "Data has been fetched";
    const STATUS = "Data status has been toggled";
    const DESTROY = "Data has been deleted";
    const LOGGED_IN = "Logged in Successfully";
    const LOGGED_OUT = "Logged out Successfully";
}
