<?php

namespace App\Traits;

use App\Enums\ApiStatus;
use App\Helper\Helper;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait RequestValidator
{

    public function failedValidation(Validator $validator){
        $helper=new Helper();
        $response = $helper->getApiResponse( ApiStatus::Failure, "Invalid input data", [ 'errors' => $validator->errors()] );
        throw new HttpResponseException($response);
    }
}
