<?php

namespace App\Helper;

use App\Enums\ApiStatus;

class Helper{

    public static function getObject($model, $request)
    {
        $data = $request->only($model->getFillable());
        $model->fill($data);
        return $model;
    }

    public static function getApiResponse($status, $message, $data=null){
        $response =  [
            "status" => ApiStatus::getDescription($status),
            "code" => $status,
            "message" => $message,
            "data" => $data
        ];
        return response()->json($response, 200);
    }
}
