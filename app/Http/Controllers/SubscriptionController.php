<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Service\Interfaces\ISubscriptionService;
use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Enums\ApiStatus;
use App\Enums\Message;
use App\Exceptions\ClientException;
use App\Http\Requests\SubscriptionRequest;
use Exception;

class SubscriptionController extends Controller
{
    public function __construct(ISubscriptionService $subscriptionService)
    {
        $this->helper = new Helper();
        $this->subscriptionService = $subscriptionService;
    }

    public function add(SubscriptionRequest $request)
    {
        try {
            $subscription = new Subscription();
            $subscription = $this->helper->getObject($subscription, $request);
            $subscription = $this->subscriptionService->add($subscription, $request);
            return $this->helper->getApiResponse(ApiStatus::Success, Message::ADDED, ['subscription' => $subscription]);
        } catch (ClientException $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, $e->getMessage(), null);
        } catch (Exception $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, Message::FAILED, null);
        }
    }

    public function toggle($id)
    {
        try {
            $subscription = $this->subscriptionService->toggle($id);
            return $this->helper->getApiResponse(ApiStatus::Success, Message::STATUS, ['subscription' => $subscription]);
        } catch (ClientException $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, $e->getMessage(), null);
        } catch (Exception $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, Message::FAILED, null);
        }
    }
}
