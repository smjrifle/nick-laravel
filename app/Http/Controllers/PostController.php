<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Service\Interfaces\IPostService;
use App\Models\Post;
use App\Enums\ApiStatus;
use App\Enums\Message;
use App\Exceptions\ClientException;
use App\Http\Requests\PostRequest;
use Exception;

class PostController extends Controller
{
    public function __construct(IPostService $postService)
    {
        $this->helper = new Helper();
        $this->postService = $postService;
    }

    public function add(PostRequest $request)
    {
        try {
            $post = new Post();
            $post = $this->helper->getObject($post, $request);
            $post = $this->postService->add($post);
            return $this->helper->getApiResponse(ApiStatus::Success, Message::ADDED, ['post' => $post]);
        } catch (ClientException $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, $e->getMessage(), null);
        } catch (Exception $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, Message::FAILED, null);
        }
    }
}
