<?php

namespace App\Http\Controllers;

use App\Enums\ApiStatus;
use App\Enums\Message;
use App\Helper\Helper;
use App\Http\Requests\WebsiteRequest;
use App\Models\Website;
use App\Service\Interfaces\IWebsiteService;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function __construct(IWebsiteService $websiteService)
    {
        $this->helper = new Helper();
        $this->websiteService = $websiteService;
    }

    public function add(WebsiteRequest $request)
    {
        try {
            $website = new Website();
            $website = $this->helper->getObject($website, $request);
            $website = $this->websiteService->add($website);
            return $this->helper->getApiResponse(ApiStatus::Success, Message::ADDED, ['website' => $website]);
        } catch (ClientException $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, $e->getMessage(), null);
        } catch (Exception $e) {
            return $this->helper->getApiResponse(ApiStatus::Failure, Message::FAILED, null);
        }
    }
}
