<?php

namespace App\Http\Requests;

use App\Traits\RequestValidator;
use Illuminate\Foundation\Http\FormRequest;

class WebsiteRequest extends FormRequest
{
    use RequestValidator;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'website_name.required' => 'Required',
        ];
    }
}
