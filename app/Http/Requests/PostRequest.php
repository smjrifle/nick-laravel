<?php

namespace App\Http\Requests;

use App\Traits\RequestValidator;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{

    use RequestValidator;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'website_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Required',
            'description.required' => 'Required',
        ];
    }
}
