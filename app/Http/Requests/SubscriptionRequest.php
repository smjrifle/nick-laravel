<?php

namespace App\Http\Requests;

use App\Traits\RequestValidator;
use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    use RequestValidator;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website_id' => 'required',
            'email_id' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'website_id.required' => 'Required',
            'email_id.required' => 'Required',
            'email_id.email' => 'Invalid Email'
        ];
    }
}
