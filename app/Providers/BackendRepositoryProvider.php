<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repository\Interfaces\IPostRepository',
            'App\Repository\PostRepository'
        );

        $this->app->bind(
            'App\Repository\Interfaces\ISubscriptionRepository',
            'App\Repository\SubscriptionRepository'
        );

        $this->app->bind(
            'App\Repository\Interfaces\IWebsiteRepository',
            'App\Repository\WebsiteRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
