<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Service\Interfaces\IPostService',
            'App\Service\PostService'
        );

        $this->app->bind(
            'App\Service\Interfaces\ISubscriptionService',
            'App\Service\SubscriptionService'
        );

        $this->app->bind(
            'App\Service\Interfaces\IWebsiteService',
            'App\Service\WebsiteService'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
