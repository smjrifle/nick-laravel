<?php

namespace App\Exceptions;

use App\Enums\ApiStatus;
use App\Functions\Class360;
use Exception;
use Illuminate\Support\Facades\Log;

class ClientException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        Log::error($exception);
    }
}
