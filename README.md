Steps to run the project
1. git clone https://github.com/smjrifle/nick-laravel.git and run composer install
2. Create .env file from .env.example 
3. php artisan optimize
4. Run the migrations via php artisan migrate
5. Seed the DB via php artisan db:seed
6. The API endpoints are
    6.1 http://localhost:8000/api/post/add (data: {
    "title": "Test",
    "description": "Test",
    "website_id": 3
    })
    6.2 http://localhost:8000/api/website/add (data: {
    "website_name": "Test"
    })
    6.3 http://localhost:8000/api/subscription/add (data: {
    "website_id": 3,
    "email_id": "smjrifle@gmail.com"
    }) //Added Email Validation and if the data exists it makes inactive subscription active
    6.4 http://localhost:8000/api/subscription/toggle/subscription_id //To toggle subscription on/off
7. Edit your SMTP settings on .env file
8. Run php artisan queue:work to open email queue
9. Add a post and it should send an email to active subscription after post
